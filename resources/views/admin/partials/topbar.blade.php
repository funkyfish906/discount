<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner">
        <div class="page-header-inner">
            <div class="navbar-header">
                <a href="{{ url(config('quickadmin.homeRoute')) }}" class="navbar-brand">
                    {{ trans('quickadmin::admin.partials-topbar-title') }}
                </a>
            </div>
            <a href="javascript:;"
               class="menu-toggler responsive-toggler"
               data-toggle="collapse"
               data-target=".navbar-collapse">
            </a>

            <div class="top-menu pull-left">
                <ul class="nav navbar-nav">
                    <li><a href="#">Инструкции</a></li>
                    <li><a href="#">Как это работает</a></li>
                    <li><a href="#">Контакты</a></li>
                    <li><a href="#">Настройки</a></li>
                </ul>
            </div>

            <div class="profile-menu pull-right">
                {{Auth::user()->name}}
                {!! Form::open(['url' => 'logout']) !!}
                <button type="submit" class="logout">
                    <i class="fa fa-sign-out fa-fw"></i>
                    <span class="title">{{ trans('quickadmin::admin.partials-sidebar-logout') }}</span>
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>