@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
       

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="info-block">
            Название торговой точки и логотип
            
            <h1><img class="logo" src="/uploads/thumb/{{$tradepoints->logo}}" />  {{$tradepoints->name}}</h1>
        </div>
        <div class="info-block">
            Краткое описание до 256 символов
            {{$tradepoints->description}}
        </div>
    </div>
    <div class="col-md-6">
        <div class="info-block">
            Подключен к DISCOUNTU
        </div>
        <div class="info-block">
            Фотогалерея
            @foreach ($gallery as $photo)
            <img class="logo" src="/uploads/thumb/{{($photo->photo)}}" />
            @endforeach
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="info-block">
            Адресс и контактные данные: </br>
            <div class="field">
                <span class="label-text">Страна</span>
                <span class="value-text">{{$tradepoints->country}}</span>
            </div>
            <div class="field">
                <span class="label-text">Город</span>
                <span class="value-text">{{$tradepoints->city}}</span>
            </div>
            <div class="field">
                <span class="label-text">Улица</span>
                <span class="value-text">{{$tradepoints->address}}</span>
            </div>
            <div class="field">
                <span class="label-text">Примечание</span>
                <span class="value-text">{{$tradepoints->footnote}}</span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="info-block">
            <div class="field">
                <span class="label-text">Телефон</span>
                <span class="value-text">{{$tradepoints->phone}}</span>
            </div>
            <div class="field">
                <span class="label-text">Електронная почта</span>
                <span class="value-text">{{$tradepoints->email}}</span>
            </div>
            <div class="field">
                <span class="label-text">Сайт</span>
                <span class="value-text">{{$tradepoints->website}}</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6"> 
        <div class="info-block">
        
            График работы торговой точки: <br />

            @foreach ($tradepoints->schedule as $val)
            <div class="field">
                <span class="label-text">{{$val->day->day}}</span>
                <span class="value-text">{{$val->time}}</span>
            </div>
            @endforeach
        
        </div>
    </div>
    <div class="col-md-6">
        <div class="info-block">
            @foreach ($tradepoints->schedule as $val)
            @if ($val->break != null)
            <div class="field">
                <span class="label-text">Перерыв</span>
                <span class="value-text">{{$val->break}}</span>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<!-- 
{!! Form::model($tradepoints, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.tradepoints.update', $tradepoints->id))) !!}

<div class="form-group">
    {!! Form::label('name', 'Название торговой точки*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('name', old('name',$tradepoints->name), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('logo', 'Логотип', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('logo') !!}
        {!! Form::hidden('logo_w', 4096) !!}
        {!! Form::hidden('logo_h', 4096) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('connected', 'Подключен к Discount*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::hidden('connected','') !!}
        {!! Form::checkbox('connected', 1, $tradepoints->connected == 1) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('description', 'Короткое описание компании (до 255 символов)', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::textarea('description', old('description',$tradepoints->description), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('country', 'Страна*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('country', old('country',$tradepoints->country), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('city', 'Город*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('city', old('city',$tradepoints->city), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('address', 'Улица', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('address', old('address',$tradepoints->address), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('footnote', 'Примечание', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('footnote', old('footnote',$tradepoints->footnote), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('phone', 'Телефон*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('phone', old('phone',$tradepoints->phone), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('email', 'Електронная почта*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('email', old('email',$tradepoints->email), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('website', 'Сайт', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('website', old('website',$tradepoints->website), array('class'=>'form-control')) !!}
        
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
      {!! link_to_route(config('quickadmin.route').'.tradepoints.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
    </div>
</div>

{!! Form::close() !!} -->

@endsection