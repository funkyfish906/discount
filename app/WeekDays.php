<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


class WeekDays extends Model
{

	protected $table    = 'week_days';

	protected $primaryKey = 'id';
    
    protected $fillable = [
          'id',
          'day',
    ];


    public static function boot()
    {
        parent::boot();

        WeekDays::observe(new UserActionsObserver);
    }

/*    public function day()
    {
        return $this->belongsTo(WeekDays::class, 'day_id');
    }*/
}
