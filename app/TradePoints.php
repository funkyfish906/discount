<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


//use Illuminate\Database\Eloquent\SoftDeletes;

class TradePoints extends Model {

    //use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    //protected $dates = ['deleted_at'];
    
    protected $primaryKey = 'id';

    protected $table    = 'tradepoints';
    
    protected $fillable = [
          'name',
          'logo',
          'connected',
          'description',
          'country',
          'city',
          'address',
          'footnote',
          'phone',
          'email',
          'website'
    ];
    

    public static function boot()
    {
        parent::boot();

        TradePoints::observe(new UserActionsObserver);
    }
    
    public function schedule()
    {
        return $this->hasMany(Schedule::class, 'company_id');
    }
    
    
}