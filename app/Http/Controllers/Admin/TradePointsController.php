<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Http\Requests\CreateTradePointsRequest;
use App\Http\Requests\UpdateTradePointsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\TradePoints;
use App\PhotoGallery;
use App\WeekDays;
use App\Schedule;


class TradePointsController extends Controller {

	/**
	 * Display a listing of tradepoints
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $tradepoints = TradePoints::all();

		return view('admin.tradepoints.index', compact('tradepoints'));
	}

	/**
	 * Show the form for creating a new tradepoints
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $weekDays = WeekDays::all();
	    
	    return view('admin.tradepoints.create')->with(['weekdays' => $weekDays]);
	}

	/**
	 * Store a newly created tradepoints in storage.
	 *
     * @param CreateTradePointsRequest|Request $request
	 */
	public function store(CreateTradePointsRequest $request)
	{
		$data = $request->all();
		
	    $request = $this->saveFiles($request);
	    $tradePoint = new TradePoints;
	    $tradePoint->name = $data['name'];
	    $tradePoint->connected = $data['connected'];	   
	    $tradePoint->description = $data['description'];
	    $tradePoint->country = $data['country'];
	    $tradePoint->city = $data['city'];
	    $tradePoint->address = $data['address'];
	    $tradePoint->footnote = $data['footnote'];
	    $tradePoint->phone = $data['phone'];
	    $tradePoint->email = $data['email'];
	    $tradePoint->website = $data['website'];

		$tradePoint->save();

		$pointId = $tradePoint->id;

		$days = $data['time'];

		foreach ($days as $index=>$time){
				$sсhedule = new Schedule;

	    		$sсhedule->company_id = $pointId;
	    		$sсhedule->day_id = $index; 
	    		$sсhedule->time = $data['time'][$index][0];
	    		$sсhedule->break = $data['break'][$index][0];
				$sсhedule->save();
			
		}

		return redirect()->route(config('quickadmin.route').'.tradepoints.index');
	}

	/**
	 * Show the form for editing the specified tradepoints.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tradepoints = TradePoints::find($id);	

		$weekDays = WeekDays::all();

		$schedule = Schedule::where('company_id',$id)->get();

		return view('admin.tradepoints.edit')->with(['tradepoints' => $tradepoints, 'weekdays' => $weekDays, 'schedule' => $schedule]);
	}

	/**
	 * Update the specified tradepoints in storage.
     * @param UpdateTradePointsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTradePointsRequest $request)
	{
		$data = $request->all();

		$tradepoints = TradePoints::findOrFail($id);

        $request = $this->saveFiles($request);

		$tradepoints->update($data);

		$oldDays = Schedule::where('company_id',$id)->delete();

		//dump($oldDays); die();

		$days = $data['time'];

		foreach ($days as $index=>$time){
				$shedule = new Schedule;

	    		$shedule->company_id = $id;
	    		$shedule->day_id = $index; 
	    		$shedule->time = $data['time'][$index][0];
	    		$shedule->break = $data['break'][$index][0];
				$shedule->save();
			
		}

		return redirect()->route(config('quickadmin.route').'.tradepoints.index');
	}

	/**
	 * Remove the specified tradepoints from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		TradePoints::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tradepoints.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            TradePoints::destroy($toDelete);
        } else {
            TradePoints::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tradepoints.index');
    }


    public function show($id)
	{
		$tradepoints = TradePoints::find($id);
		$gallery = PhotoGallery::where('company_id',$id)->get();
	    
	    
		return view('admin.tradepoints.show')->with(['tradepoints' => $tradepoints, 'gallery' => $gallery]);
	}

}
