<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\PhotoGallery;
use App\Http\Requests\CreatePhotoGalleryRequest;
use App\Http\Requests\UpdatePhotoGalleryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\TradePoints;


class PhotoGalleryController extends Controller {

	/**
	 * Display a listing of photogallery
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $photogallery = PhotoGallery::all();

		return view('admin.photogallery.index', compact('photogallery'));
	}

	/**
	 * Show the form for creating a new photogallery
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $companies = TradePoints::pluck('name', 'id');
	    
	    return view('admin.photogallery.create')->with(['companies' => $companies]);
	}

	/**
	 * Store a newly created photogallery in storage.
	 *
     * @param CreatePhotoGalleryRequest|Request $request
	 */
	public function store(CreatePhotoGalleryRequest $request)
	{
	    $request = $this->saveFiles($request);
		PhotoGallery::create($request->all());

		return redirect()->route(config('quickadmin.route').'.photogallery.index');
	}

	/**
	 * Show the form for editing the specified photogallery.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$photogallery = PhotoGallery::find($id);
		$companies = TradePoints::pluck('name', 'id');
	    
	    
		return view('admin.photogallery.edit')->with(['photogallery' => $photogallery, 'companies' => $companies]);
	}

	/**
	 * Update the specified photogallery in storage.
     * @param UpdatePhotoGalleryRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdatePhotoGalleryRequest $request)
	{
		$photogallery = PhotoGallery::findOrFail($id);

        $request = $this->saveFiles($request);

		$photogallery->update($request->all());

		return redirect()->route(config('quickadmin.route').'.photogallery.index');
	}

	/**
	 * Remove the specified photogallery from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		PhotoGallery::destroy($id);

		return redirect()->route(config('quickadmin.route').'.photogallery.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            PhotoGallery::destroy($toDelete);
        } else {
            PhotoGallery::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.photogallery.index');
    }

}
