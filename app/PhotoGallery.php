<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class PhotoGallery extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'photogallery';

    protected $primaryKay    = 'id';
    
    protected $fillable = [
          'photo',
          'company_id'
    ];
    

    public static function boot()
    {
        parent::boot();

        PhotoGallery::observe(new UserActionsObserver);
    }
    
    public function company()
    {
        return $this->belongsTo(TradePoints::class, 'company_id');
    }
    
    
    
}