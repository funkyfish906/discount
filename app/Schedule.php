<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


class Schedule extends Model {

    //use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    //protected $dates = ['deleted_at'];

    protected $table    = 'schedule';

    protected $primaryKey    = 'id';
    
    protected $fillable = [
          'company_id',
          'day_id',
          'time',
          'break'
    ];
    

    public static function boot()
    {
        parent::boot();

        Schedule::observe(new UserActionsObserver);
    }
    

    public function company()
    {
        return $this->belongsTo(TradePoints::class, 'company_id');
    }
    
    public function day()
    {
        return $this->belongsTo(WeekDays::class, 'day_id');
    }
    
    
    
}