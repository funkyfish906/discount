<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateTradePointsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('tradepoints',function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("logo")->nullable();
            $table->string("connected")->nullable();
            $table->text("description")->nullable();
            $table->string("country")->nullable();
            $table->string("city")->nullable();
            $table->string("address")->nullable();
            $table->string("footnote")->nullable();
            $table->string("phone")->nullable();
            $table->string("email")->nullable();
            $table->string("website")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tradepoints');
    }

}