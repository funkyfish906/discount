-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 17 2017 г., 17:35
-- Версия сервера: 5.7.16
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `discount_2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` int(11) DEFAULT NULL,
  `menu_type` int(11) NOT NULL DEFAULT '1',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `position`, `menu_type`, `icon`, `name`, `title`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 0, NULL, 'User', 'User', NULL, NULL, NULL),
(2, NULL, 0, NULL, 'Role', 'Role', NULL, NULL, NULL),
(3, 0, 1, 'fa-database', 'TradePoints', 'TradePoints', 6, '2017-05-16 08:25:50', '2017-05-16 08:37:19'),
(4, 0, 1, 'fa-database', 'PhotoGallery', 'PhotoGallery', NULL, '2017-05-16 08:30:06', '2017-05-16 08:30:06'),
(6, 0, 2, 'fa-database', 'CompaniesInformation', 'Companies Information', NULL, '2017-05-16 08:36:59', '2017-05-16 08:36:59'),
(7, 0, 1, 'fa-database', 'Schedule', 'Schedule', NULL, '2017-05-16 18:00:51', '2017-05-16 18:00:51');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_role`
--

CREATE TABLE `menu_role` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_role`
--

INSERT INTO `menu_role` (`menu_id`, `role_id`) VALUES
(3, 1),
(4, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_10_10_000000_create_menus_table', 1),
(4, '2015_10_10_000000_create_roles_table', 1),
(5, '2015_10_10_000000_update_users_table', 1),
(6, '2015_12_11_000000_create_users_logs_table', 1),
(7, '2016_03_14_000000_update_menus_table', 1),
(8, '2017_05_16_112550_create_tradepoints_table', 2),
(9, '2017_05_16_113006_create_photogallery_table', 3),
(11, '2017_05_16_114826_create_weekdays_table', 5),
(12, '2017_05_16_210051_create_schedule_table', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `photogallery`
--

CREATE TABLE `photogallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `photogallery`
--

INSERT INTO `photogallery` (`id`, `photo`, `company_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1494934226-Chrysanthemum.jpg', '29', '2017-05-16 08:30:26', '2017-05-16 08:30:26', NULL),
(2, '1494934237-Koala.jpg', '29', '2017-05-16 08:30:38', '2017-05-16 08:30:38', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2017-05-16 07:36:20', '2017-05-16 07:36:20'),
(2, 'User', '2017-05-16 07:36:20', '2017-05-16 07:36:20');

-- --------------------------------------------------------

--
-- Структура таблицы `schedule`
--

CREATE TABLE `schedule` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `day_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `break` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `schedule`
--

INSERT INTO `schedule` (`id`, `company_id`, `day_id`, `time`, `break`, `created_at`, `updated_at`) VALUES
(78, 29, '1', '10:00-22:00', '13:00-13:30', '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(79, 29, '2', '12:00-20:00', '14:00-14:30', '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(80, 29, '3', '12:00-20:00', 'цілодобово', '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(81, 29, '4', '10:00-22:00', '14:00-14:30', '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(82, 29, '5', '10:00-22:00', 'цілодобово', '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(83, 29, '6', '10:00-22:00', 'цілодобово', '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(84, 29, '7', NULL, NULL, '2017-05-17 11:33:26', '2017-05-17 11:33:26');

-- --------------------------------------------------------

--
-- Структура таблицы `tradepoints`
--

CREATE TABLE `tradepoints` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connected` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footnote` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tradepoints`
--

INSERT INTO `tradepoints` (`id`, `name`, `logo`, `connected`, `description`, `country`, `city`, `address`, `footnote`, `phone`, `email`, `website`, `created_at`, `updated_at`) VALUES
(29, 'Сакура', 'D:\\OpenServer\\userdata\\temp\\php58CA.tmp', '1', 'суши бар', 'Украина', 'Хмельницкий', 'Пушкина 13', '2ой этаж ТЦ \"Майдан\"', '555-555', 'sacura@gmail.com', 'sacura.ua', '2017-05-17 10:22:55', '2017-05-17 11:33:26');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@gmail.com', '$2y$10$l3FQu1XDhGpL5VIW.uWNXOVrVK7c1ft2PfYEbxoG2EJfEIc0HFUea', 'gOALYvWUyLQqY1SfjSkoMfKiPbwq5H9qzB7qzC5R8nLQJ7vC6QkNFCdPVv9m', '2017-05-16 07:36:55', '2017-05-16 07:36:55');

-- --------------------------------------------------------

--
-- Структура таблицы `users_logs`
--

CREATE TABLE `users_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users_logs`
--

INSERT INTO `users_logs` (`id`, `user_id`, `action`, `action_model`, `action_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'updated', 'users', 1, '2017-05-16 07:37:58', '2017-05-16 07:37:58'),
(2, 1, 'created', 'tradepoints', 1, '2017-05-16 08:27:23', '2017-05-16 08:27:23'),
(3, 1, 'created', 'photogallery', 1, '2017-05-16 08:30:26', '2017-05-16 08:30:26'),
(4, 1, 'created', 'photogallery', 2, '2017-05-16 08:30:38', '2017-05-16 08:30:38'),
(5, 1, 'created', 'schedule', 1, '2017-05-16 08:34:37', '2017-05-16 08:34:37'),
(6, 1, 'created', 'schedule', 1, '2017-05-16 18:01:15', '2017-05-16 18:01:15'),
(7, 1, 'created', 'schedule', 2, '2017-05-16 18:01:39', '2017-05-16 18:01:39'),
(8, 1, 'updated', 'schedule', 2, '2017-05-16 18:02:03', '2017-05-16 18:02:03'),
(9, 1, 'created', 'schedule', 3, '2017-05-16 18:27:04', '2017-05-16 18:27:04'),
(10, 1, 'updated', 'schedule', 2, '2017-05-16 18:35:20', '2017-05-16 18:35:20'),
(11, 1, 'created', 'tradepoints', 2, '2017-05-16 19:58:14', '2017-05-16 19:58:14'),
(12, 1, 'created', 'tradepoints', 3, '2017-05-16 20:09:41', '2017-05-16 20:09:41'),
(13, 1, 'created', 'tradepoints', 4, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(14, 1, 'created', 'schedule', 4, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(15, 1, 'created', 'schedule', 5, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(16, 1, 'created', 'schedule', 6, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(17, 1, 'created', 'schedule', 7, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(18, 1, 'created', 'schedule', 8, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(19, 1, 'created', 'schedule', 9, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(20, 1, 'created', 'schedule', 10, '2017-05-16 20:14:59', '2017-05-16 20:14:59'),
(21, 1, 'created', 'tradepoints', 5, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(22, 1, 'created', 'schedule', 11, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(23, 1, 'created', 'schedule', 12, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(24, 1, 'created', 'schedule', 13, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(25, 1, 'created', 'schedule', 14, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(26, 1, 'created', 'schedule', 15, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(27, 1, 'created', 'schedule', 16, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(28, 1, 'created', 'schedule', 17, '2017-05-16 20:16:28', '2017-05-16 20:16:28'),
(29, 1, 'created', 'tradepoints', 6, '2017-05-16 20:24:45', '2017-05-16 20:24:45'),
(30, 1, 'created', 'schedule', 18, '2017-05-16 20:24:45', '2017-05-16 20:24:45'),
(31, 1, 'created', 'schedule', 19, '2017-05-16 20:24:45', '2017-05-16 20:24:45'),
(32, 1, 'created', 'schedule', 20, '2017-05-16 20:24:45', '2017-05-16 20:24:45'),
(33, 1, 'created', 'tradepoints', 7, '2017-05-16 20:27:03', '2017-05-16 20:27:03'),
(34, 1, 'created', 'tradepoints', 8, '2017-05-16 20:27:38', '2017-05-16 20:27:38'),
(35, 1, 'created', 'tradepoints', 9, '2017-05-16 20:28:21', '2017-05-16 20:28:21'),
(36, 1, 'created', 'schedule', 21, '2017-05-16 20:28:21', '2017-05-16 20:28:21'),
(37, 1, 'created', 'schedule', 22, '2017-05-16 20:28:21', '2017-05-16 20:28:21'),
(38, 1, 'created', 'schedule', 23, '2017-05-16 20:28:21', '2017-05-16 20:28:21'),
(39, 1, 'created', 'tradepoints', 10, '2017-05-16 20:29:40', '2017-05-16 20:29:40'),
(40, 1, 'created', 'schedule', 24, '2017-05-16 20:29:40', '2017-05-16 20:29:40'),
(41, 1, 'created', 'schedule', 25, '2017-05-16 20:29:40', '2017-05-16 20:29:40'),
(42, 1, 'created', 'schedule', 26, '2017-05-16 20:29:40', '2017-05-16 20:29:40'),
(43, 1, 'created', 'tradepoints', 11, '2017-05-16 20:30:35', '2017-05-16 20:30:35'),
(44, 1, 'created', 'schedule', 27, '2017-05-16 20:30:35', '2017-05-16 20:30:35'),
(45, 1, 'created', 'schedule', 28, '2017-05-16 20:30:35', '2017-05-16 20:30:35'),
(46, 1, 'created', 'schedule', 29, '2017-05-16 20:30:35', '2017-05-16 20:30:35'),
(47, 1, 'created', 'tradepoints', 12, '2017-05-17 04:14:47', '2017-05-17 04:14:47'),
(48, 1, 'created', 'tradepoints', 13, '2017-05-17 04:18:41', '2017-05-17 04:18:41'),
(49, 1, 'created', 'tradepoints', 14, '2017-05-17 04:20:37', '2017-05-17 04:20:37'),
(50, 1, 'created', 'tradepoints', 15, '2017-05-17 04:22:20', '2017-05-17 04:22:20'),
(51, 1, 'created', 'schedule', 30, '2017-05-17 04:22:20', '2017-05-17 04:22:20'),
(52, 1, 'created', 'tradepoints', 16, '2017-05-17 04:22:32', '2017-05-17 04:22:32'),
(53, 1, 'created', 'schedule', 31, '2017-05-17 04:22:32', '2017-05-17 04:22:32'),
(54, 1, 'created', 'schedule', 32, '2017-05-17 04:22:32', '2017-05-17 04:22:32'),
(55, 1, 'created', 'schedule', 33, '2017-05-17 04:22:32', '2017-05-17 04:22:32'),
(56, 1, 'created', 'schedule', 34, '2017-05-17 04:22:32', '2017-05-17 04:22:32'),
(57, 1, 'created', 'schedule', 35, '2017-05-17 04:22:32', '2017-05-17 04:22:32'),
(58, 1, 'created', 'schedule', 36, '2017-05-17 04:22:32', '2017-05-17 04:22:32'),
(59, 1, 'created', 'tradepoints', 17, '2017-05-17 04:22:56', '2017-05-17 04:22:56'),
(60, 1, 'created', 'tradepoints', 18, '2017-05-17 04:23:24', '2017-05-17 04:23:24'),
(61, 1, 'created', 'tradepoints', 19, '2017-05-17 04:24:10', '2017-05-17 04:24:10'),
(62, 1, 'created', 'tradepoints', 20, '2017-05-17 04:24:29', '2017-05-17 04:24:29'),
(63, 1, 'created', 'tradepoints', 21, '2017-05-17 04:25:24', '2017-05-17 04:25:24'),
(64, 1, 'created', 'tradepoints', 22, '2017-05-17 04:26:35', '2017-05-17 04:26:35'),
(65, 1, 'created', 'tradepoints', 23, '2017-05-17 04:28:23', '2017-05-17 04:28:23'),
(66, 1, 'created', 'schedule', 37, '2017-05-17 04:28:24', '2017-05-17 04:28:24'),
(67, 1, 'created', 'schedule', 38, '2017-05-17 04:28:24', '2017-05-17 04:28:24'),
(68, 1, 'created', 'schedule', 39, '2017-05-17 04:28:24', '2017-05-17 04:28:24'),
(69, 1, 'created', 'schedule', 40, '2017-05-17 04:28:24', '2017-05-17 04:28:24'),
(70, 1, 'created', 'schedule', 41, '2017-05-17 04:28:24', '2017-05-17 04:28:24'),
(71, 1, 'created', 'schedule', 42, '2017-05-17 04:28:24', '2017-05-17 04:28:24'),
(72, 1, 'created', 'schedule', 43, '2017-05-17 04:28:24', '2017-05-17 04:28:24'),
(73, 1, 'created', 'tradepoints', 24, '2017-05-17 04:29:52', '2017-05-17 04:29:52'),
(74, 1, 'created', 'schedule', 44, '2017-05-17 04:29:52', '2017-05-17 04:29:52'),
(75, 1, 'created', 'schedule', 45, '2017-05-17 04:29:52', '2017-05-17 04:29:52'),
(76, 1, 'created', 'schedule', 46, '2017-05-17 04:29:52', '2017-05-17 04:29:52'),
(77, 1, 'created', 'tradepoints', 25, '2017-05-17 04:30:54', '2017-05-17 04:30:54'),
(78, 1, 'created', 'schedule', 47, '2017-05-17 04:30:54', '2017-05-17 04:30:54'),
(79, 1, 'created', 'schedule', 48, '2017-05-17 04:30:54', '2017-05-17 04:30:54'),
(80, 1, 'created', 'schedule', 49, '2017-05-17 04:30:54', '2017-05-17 04:30:54'),
(81, 1, 'created', 'tradepoints', 26, '2017-05-17 04:31:57', '2017-05-17 04:31:57'),
(82, 1, 'created', 'schedule', 50, '2017-05-17 04:31:57', '2017-05-17 04:31:57'),
(83, 1, 'created', 'schedule', 51, '2017-05-17 04:31:57', '2017-05-17 04:31:57'),
(84, 1, 'created', 'schedule', 52, '2017-05-17 04:31:57', '2017-05-17 04:31:57'),
(85, 1, 'created', 'tradepoints', 27, '2017-05-17 04:47:18', '2017-05-17 04:47:18'),
(86, 1, 'created', 'tradepoints', 28, '2017-05-17 04:47:40', '2017-05-17 04:47:40'),
(87, 1, 'created', 'schedule', 53, '2017-05-17 04:47:40', '2017-05-17 04:47:40'),
(88, 1, 'created', 'schedule', 54, '2017-05-17 04:47:40', '2017-05-17 04:47:40'),
(89, 1, 'created', 'schedule', 55, '2017-05-17 04:47:40', '2017-05-17 04:47:40'),
(90, 1, 'created', 'schedule', 56, '2017-05-17 04:47:40', '2017-05-17 04:47:40'),
(91, 1, 'created', 'tradepoints', 29, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(92, 1, 'created', 'schedule', 57, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(93, 1, 'created', 'schedule', 58, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(94, 1, 'created', 'schedule', 59, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(95, 1, 'created', 'schedule', 60, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(96, 1, 'created', 'schedule', 61, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(97, 1, 'created', 'schedule', 62, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(98, 1, 'created', 'schedule', 63, '2017-05-17 10:22:55', '2017-05-17 10:22:55'),
(99, 1, 'updated', 'tradepoints', 29, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(100, 1, 'created', 'schedule', 64, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(101, 1, 'created', 'schedule', 65, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(102, 1, 'created', 'schedule', 66, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(103, 1, 'created', 'schedule', 67, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(104, 1, 'created', 'schedule', 68, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(105, 1, 'created', 'schedule', 69, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(106, 1, 'created', 'schedule', 70, '2017-05-17 10:23:35', '2017-05-17 10:23:35'),
(107, 1, 'updated', 'tradepoints', 29, '2017-05-17 10:28:44', '2017-05-17 10:28:44'),
(108, 1, 'updated', 'tradepoints', 29, '2017-05-17 10:30:15', '2017-05-17 10:30:15'),
(109, 1, 'updated', 'tradepoints', 29, '2017-05-17 10:30:48', '2017-05-17 10:30:48'),
(110, 1, 'updated', 'tradepoints', 29, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(111, 1, 'created', 'schedule', 71, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(112, 1, 'created', 'schedule', 72, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(113, 1, 'created', 'schedule', 73, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(114, 1, 'created', 'schedule', 74, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(115, 1, 'created', 'schedule', 75, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(116, 1, 'created', 'schedule', 76, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(117, 1, 'created', 'schedule', 77, '2017-05-17 10:40:39', '2017-05-17 10:40:39'),
(118, 1, 'updated', 'tradepoints', 29, '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(119, 1, 'created', 'schedule', 78, '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(120, 1, 'created', 'schedule', 79, '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(121, 1, 'created', 'schedule', 80, '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(122, 1, 'created', 'schedule', 81, '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(123, 1, 'created', 'schedule', 82, '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(124, 1, 'created', 'schedule', 83, '2017-05-17 11:33:26', '2017-05-17 11:33:26'),
(125, 1, 'created', 'schedule', 84, '2017-05-17 11:33:26', '2017-05-17 11:33:26');

-- --------------------------------------------------------

--
-- Структура таблицы `week_days`
--

CREATE TABLE `week_days` (
  `id` int(10) UNSIGNED NOT NULL,
  `day` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `week_days`
--

INSERT INTO `week_days` (`id`, `day`) VALUES
(1, 'Понедельник'),
(2, 'Вторник'),
(3, 'Среда'),
(4, 'Четверг'),
(5, 'Пятница'),
(6, 'Суббота'),
(7, 'Воскресенье');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_role`
--
ALTER TABLE `menu_role`
  ADD UNIQUE KEY `menu_role_menu_id_role_id_unique` (`menu_id`,`role_id`),
  ADD KEY `menu_role_menu_id_index` (`menu_id`),
  ADD KEY `menu_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `photogallery`
--
ALTER TABLE `photogallery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tradepoints`
--
ALTER TABLE `tradepoints`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Индексы таблицы `users_logs`
--
ALTER TABLE `users_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `week_days`
--
ALTER TABLE `week_days`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `photogallery`
--
ALTER TABLE `photogallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT для таблицы `tradepoints`
--
ALTER TABLE `tradepoints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users_logs`
--
ALTER TABLE `users_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT для таблицы `week_days`
--
ALTER TABLE `week_days`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `menu_role`
--
ALTER TABLE `menu_role`
  ADD CONSTRAINT `menu_role_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
